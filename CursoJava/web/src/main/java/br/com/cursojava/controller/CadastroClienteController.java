package br.com.cursojava.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.cursoJava.domain.Cliente;
import br.com.cursoJava.domain.exception.ClienteException;
import br.com.cursoJava.domain.services.ClienteService;
import br.com.cursoJava.domain.services.Impl.ClienteServiceProvider;

@ManagedBean
@RequestScoped
public class CadastroClienteController extends GenericController implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Cliente cliente;
	private ClienteService clienteService;
	
	@PostConstruct
	public void initialize(){
		this.cliente = new Cliente();
		this.clienteService = ClienteServiceProvider.getInstance();
	}
	
	public void cadastrarCliente() {
		if (this.validate()) {
			
			try {
				this.clienteService.cadastrarCliente(this.cliente);
				super.addSuccessMessager("Cliente Cadastrado", this.cliente.toString());
				this.cliente = new Cliente();
			} catch (ClienteException e) {
				super.addErrorMessager("Ocorreu Erros ao cadastrar o cliente", e.getMessage());
			}
			
		}
	}
	
	private boolean validate(){
		boolean res = true;
		
		if (null == cliente.getNome() || cliente.getNome().isEmpty()) {
			super.addErrorMessager("Dados Inv�lidos", "O nome do cliente n�o deve ser vazio");
			res = false;
		}
		
		if (null == cliente.getCpf() || cliente.getCpf().isEmpty()) {
			super.addErrorMessager("Dados Inv�lidos", "O CPF do cliente n�o deve ser vazio");
			res = false;
		}
		
		if (null == cliente.getIdade()) {
			super.addErrorMessager("Dados Inv�lidos", "A idade do cliente n�o deve ser vazia");
			res = false;
		}
		
		if (null == cliente.getSexo() || cliente.getSexo().isEmpty()) {
			super.addErrorMessager("Dados Inv�lidos", "O sexo do cliente n�o deve ser vazio");
			res = false;
		}
		
		return res;
	}
	
	//Getters and Setters
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
}
