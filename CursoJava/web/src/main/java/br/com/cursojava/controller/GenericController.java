package br.com.cursojava.controller;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public abstract class GenericController {
	
	protected void addErrorMessager(String title, String message){
		getCurretInstance().addMessage(null, createFacesMessage(FacesMessage.SEVERITY_ERROR, title, message));
	}
	
	protected void addWarningMessager(String title, String message){
		getCurretInstance().addMessage(null, createFacesMessage(FacesMessage.SEVERITY_WARN, title, message));
	}
	
	protected void addSuccessMessager(String title, String message){
		getCurretInstance().addMessage(null, createFacesMessage(FacesMessage.SEVERITY_INFO, title, message));
	}

	protected FacesMessage createFacesMessage(Severity severityError, String title, String message) {
		return new FacesMessage(severityError, title, message);
	}

	protected FacesContext getCurretInstance() {
		return FacesContext.getCurrentInstance();
	}
	
}
