package br.com.cursoJava.domain.services.Impl;

import java.util.ArrayList;
import java.util.List;

import br.com.cursoJava.domain.Cliente;
import br.com.cursoJava.domain.exception.ClienteCPFInvalidoException;
import br.com.cursoJava.domain.exception.ClienteException;
import br.com.cursoJava.domain.exception.ClienteInformadoJaExisteException;
import br.com.cursoJava.domain.exception.ClienteMenorIdadeException;
import br.com.cursoJava.domain.services.ClienteService;

public class ClienteServiceProvider implements ClienteService {

	
	private static final long serialVersionUID = 5726497852438087848L;

	private static ClienteService instance;
	private static List<Cliente> clientes;
	
	private ClienteServiceProvider() {
		super();
		clientes = new ArrayList<Cliente>();
	}
	
	public static ClienteService getInstance(){
		if (null == instance){
			instance = new ClienteServiceProvider();
		}
		
		return instance;
	}

	public void cadastrarCliente(Cliente cliente) throws ClienteException {
		this.validarCliente(cliente);
		this.clientes.add(cliente);
		
		
	}
	
	private boolean VerificaSeClienteJaExiste(Cliente cliente) throws ClienteException{
		boolean found = false;
		for (Cliente current : clientes) {
			if(cliente.getCpf().equals(current.getCpf())){
				found = true;
				break;
			}
		}
		
		return found;
	}

	private void validarCliente(Cliente cliente) throws ClienteException {
		if(VerificaSeClienteJaExiste(cliente)){
			throw new ClienteInformadoJaExisteException();
		}
		
		if (cliente.getIdade() < 18){
			throw new ClienteMenorIdadeException();
		}
		
		if ("111.111.111-11".equals(cliente.getCpf()) || "11111111111".equals(cliente.getCpf())){
			throw new ClienteCPFInvalidoException();
		}
		
	}
	public void excluirCliente(Cliente cliente) {
		// TODO Auto-generated method stub

	}

	public void alterarCliente(Cliente cliente) {
		// TODO Auto-generated method stub

	}

	public List<Cliente> listarCliente(Cliente cliente) {
		// TODO Auto-generated method stub
		return null;
	}

}
