package br.com.cursoJava.domain.services;

import java.io.Serializable;
import java.util.List;

import br.com.cursoJava.domain.Cliente;
import br.com.cursoJava.domain.exception.ClienteException;

public interface ClienteService extends Serializable{
	
	
	public void cadastrarCliente(Cliente cliente) throws ClienteException ;
	
	public void excluirCliente(Cliente cliente);
	
	public void alterarCliente(Cliente cliente);
	
	public List<Cliente> listarCliente(Cliente cliente);
		
}
