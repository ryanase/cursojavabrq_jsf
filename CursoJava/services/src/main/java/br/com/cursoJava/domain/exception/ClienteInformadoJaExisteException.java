package br.com.cursoJava.domain.exception;

public class ClienteInformadoJaExisteException extends ClienteException {

	private static final long serialVersionUID = 1L;
	
	public String getMessage(){
		return "O cliente informado j� existe na base de dados";
	}
}
