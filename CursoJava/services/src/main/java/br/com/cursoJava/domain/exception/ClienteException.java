package br.com.cursoJava.domain.exception;

public abstract class ClienteException extends Exception {

	private static final long serialVersionUID = -5068483691892346592L;
	
	public abstract String getMessage();
	
}
