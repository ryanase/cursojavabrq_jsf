package br.com.cursoJava.domain.exception;

public class ClienteCPFInvalidoException extends ClienteException {

	private static final long serialVersionUID = -7068223876472438091L;

	@Override
	public String getMessage() {
		return "CPF � inv�lido";
	}

}
