package br.com.cursoJava.domain.exception;

public class ClienteMenorIdadeException extends ClienteException {

	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "Cliente menor de idade";
	}

}
